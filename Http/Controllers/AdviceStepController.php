<?php

namespace Dcms\Advices\Http\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Dcms\Advices\Models\Advice;
use Dcms\Advices\Models\AdviceStep;
use Dcms\Advices\Models\AdviceStepdetail;
use Dcms\Advices\Models\AdviceStepPeriod;
use Dcms\Advices\Models\AdviceToAdviceStep;
use Dcms\Advices\Models\AdvicesStepAlert;
use Dcms\Advices\Models\AdvicesStepIdentifier;
use Dcms\Core\Models\Languages\Language;
use Illuminate\Http\Request;
use View;
use Input;
use Session;
use Validator;
use Redirect;
use DB;
use DataTables;
use Auth;
use Form;
use DateTime;

class AdviceStepController extends Controller
{
    public static function copy($step_id, $newAdvice_id = null, $oldAdvice_id = null)
    {
        $old = AdviceStep::find($step_id);
        $new = $old->replicate();
        //save model before you recreate relations (so it has an id)
        $new->push();

        //reset relations on EXISTING MODEL (this way you can control which ones will be loaded
        $old->relations = ['adviceStepsAlert','stepIdentifiers'];
        
        //load relations on EXISTING MODEL
        $old->load('adviceStepsAlert', 'stepIdentifiers');

        //re-sync everything
        foreach ($old->relations as $relationName => $values) {
            $new->{$values}()->sync($old->$values()->get());
        }

        foreach ($old->detail()->get() as $detail) {
            $newdetail = $detail->replicate();
            $newdetail->advices_step_id = $new->id;
            $newdetail->save();
        }

        foreach ($old->product()->get() as $product) {
            $newproduct = $product->replicate();
            $newproduct->advices_step_id = $new->id;
            $newproduct->save();

            foreach ($product->detail()->get() as $productdetail) {
                $newproductdetail = $productdetail->replicate();
                $newproductdetail->products_information_group_to_advices_step_id = $newproduct->id;
                $newproductdetail->save();
            }
        }

        //get old advice //old step id
        if (!empty($newAdvice_id) && !empty($oldAdvice_id)) {
            foreach (AdviceToAdviceStep::where('advices_id', '=', $oldAdvice_id)->where('advices_step_id', '=', $old->id)->get() as $adviceToAdviceStep) {
                $newAdviceToAdviceStep = $adviceToAdviceStep->replicate();
                $newAdviceToAdviceStep->advices_id = $newAdvice_id;
                $newAdviceToAdviceStep->advices_step_id = $new->id;
                $newAdviceToAdviceStep->save();
            }
        } else {
        }

        return "copy ok the new advice step id: ".$new->id;
    }

    public function getDatatable($advice_id)
    {
        $query = DB::connection('project')
        ->table('advices_to_advices_step')
        ->select(
            'advices_to_advices_step.advices_step_id',
            'advices_to_advices_step.id',
            'advices_step_language.title as advice_step',
            'advices_to_advices_step.period_id',
            'advices_step.sort_id',
            'advices_step.once',
            'advices_step.reject',
            (DB::connection('project')->raw('(
                select group_concat(concat(products_information.title, \' - \' , products_information.catalogue) SEPARATOR \' + \')  as X
                 from products_information_group_to_advices_step left join products_information on products_information_group_to_advices_step.information_group_id = products_information.information_group_id
                where products_information_group_to_advices_step.advices_step_id = `advices_to_advices_step`.`advices_step_id` and products_information.language_id = `advices_step_language`.`language_id`
                
                ) as products')),
            (DB::connection("project")->raw('Concat("<img src=\'/packages/Dcms/Core/images/flag-",lcase(country),".png\' >") as country'))
        )
        ->join('advices_step', 'advices_to_advices_step.advices_step_id', '=', 'advices_step.id')
        ->join('advices', 'advices_to_advices_step.advices_id', '=', 'advices.id')
        ->join('advices_step_language', 'advices_step.id', '=', 'advices_step_language.advices_step_id')
        ->leftJoin('languages', 'advices_step_language.language_id', '=', 'languages.id')
        ->where('advices.id', '=', $advice_id)
        ->orderBy('advices_to_advices_step.period_id')
        ->orderBy('advices_step.sort_id')
        ->orderBy('advices_step.id')
        ->orderBy('advices_step_language.language_id');
        
        if (intval(session('overrule_default_by_language_id')) > 0) {
            $query->where('languages.id', session('overrule_default_by_language_id'));
        }
        
        return DataTables::queryBuilder($query)
            ->addColumn('edit', function ($model) {
                return '<form method="POST" action="/admin/advice_steps/' . $model->id . '" accept-charset="UTF-8" class="pull-right"> <input name="_token" type="hidden" value="' . csrf_token() . '"> <input name="_method" type="hidden" value="DELETE">
																			<a class="btn btn-xs btn-default" href="/admin/advice_steps/' . $model->id . '/edit"><i class="far fa-pencil-alt"></i></a>
																			<button class="btn btn-xs btn-default" type="submit" value="Delete this article" onclick="if(!confirm(\'Are you sure to delete this item?\r\nThis can affect other countries!!\')){return false;};"><i class="far fa-trash-alt"></i></button>
																		</form>';
            })
            ->rawColumns(['country', 'edit'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $advice_step = new AdviceStep();
        $languages = Language::whereIn('id', [1, 2, 3, 6, 7])->get();
        $periods = AdviceStepPeriod::all();
        $adviceStepAlert = AdvicesStepAlert::with('detail')->get();
        
        //get identifiers
        $stepIdentifiers = AdvicesStepIdentifier::all();

        // load the create form (app/views/advices/create.blade.php)
        return View::make('dcms::steps/form')
            ->with('advice_step', $advice_step)
            ->with('languages', $languages)
            ->with('periods', $periods)
            ->with('allActivePeriods', [])
            ->with('advice_id', $request->get('advice_id'))
            ->with('adviceStepAlert', $adviceStepAlert)
            ->with('stepIdentifiers', $stepIdentifiers)
            ->with('selectedIdentifiers', []);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $advice_to_advice_step = AdviceToAdviceStep::findOrFail($id);
        $advice_step = $advice_to_advice_step->adviceStep;
        $allActivePeriods = AdviceToAdviceStep::where('advices_id', $advice_to_advice_step->advices_id)->where('advices_step_id', $advice_to_advice_step->advices_step_id)->pluck('period_id');
        $languages = Language::whereIn('id', [1, 2, 3, 6, 7])->get();
        $periods = AdviceStepPeriod::all();
        $adviceStepAlert = AdvicesStepAlert::with('detail')->get();
        //get identifiers
        $stepIdentifiers = AdvicesStepIdentifier::all();

        return View::make('dcms::steps/form')
            ->with('advice_step', $advice_step)
            ->with('languages', $languages)
            ->with('periods', $periods)
            ->with('allActivePeriods', $allActivePeriods->toArray())
            ->with('advice_to_advice_step', $advice_to_advice_step)
            ->with('adviceStepAlert', $adviceStepAlert)
            ->with('stepIdentifiers', $stepIdentifiers)
            ->with('selectedIdentifiers', $advice_step->stepIdentifiers()->pluck('advices_step_identifier_id')->toArray());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if ($this->validateAdviceStepForm() === true) {
            $advice_to_advice_step = new AdviceToAdviceStep();
            $advice_step = new AdviceStep();

            $advice_to_advice_step = $this->processAdviceStepForm($advice_to_advice_step, $advice_step, $request);

            // redirect
            Session::flash('message', 'Successfully created step!');

            if ($request->has('create-product')) {

                $result_withoutpivot = DB::table('advices_to_advices_step')
                ->where('advices_step_id', $advice_to_advice_step->advices_step_id)
                ->where('advices_id', $advice_to_advice_step->advices_id)
                ->first();

                return redirect()->route('admin.advice_step_products.create', ['advice_to_advice_step_id' => $result_withoutpivot->id]);
            } else {
                return redirect()->route('admin.advices.edit', [$request->get('advice_id')]);
            }
        } else {
            return $this->validateAdviceStepForm();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        if ($this->validateAdviceStepForm() === true) {
            $advice_to_advice_step = AdviceToAdviceStep::findOrFail($id);
            $advice_step = $advice_to_advice_step->adviceStep;

            $advice_to_advice_step = $this->processAdviceStepForm($advice_to_advice_step, $advice_step, $request);

            // redirect
            Session::flash('message', 'Successfully updated step!');

            if ($request->has('create-product')) {
                return redirect()->route('admin.advice_step_products.create', ['advice_to_advice_step_id' => $advice_to_advice_step->id]);
            } else {
                return redirect()->route('admin.advices.edit', [$advice_step->advices->first()->id]);
            }
        } else {
            return $this->validateAdviceStepForm();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $advice_to_advice_step = AdviceToAdviceStep::findOrFail($id);
        $advice_id = $advice_to_advice_step->advices_id;
        $advice_to_advice_step->delete();

        Session::flash('message', 'Successfully deleted the step!');

        return redirect()->route('admin.advices.edit', [$advice_id]);
    }

    private function validateAdviceStepForm()
    {
        $rules = [
            //	'title' => 'required'
        ];
        $validator = Validator::make(request()->all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {
            return true;
        }
    }

    private function processAdviceStepForm($advice_to_advice_step, $advice_step, $request)
    {
        $advice_step->type_id = $request->get('type_id');
        $advice_step->parameter = $request->has('parameter');
        $advice_step->once = $request->has('once');
        $advice_step->aid = $request->has('aid');
        $advice_step->reject = $request->has('reject');
        $advice_step->sort_id = $request->get('sort_id');
        $advice_step->save();

        if ($request->has('alert')) {
            $advice_step->adviceStepsAlert()->sync(array_keys($request->get('alert')));
        } else {
            $advice_step->adviceStepsAlert()->sync([]);
        }

        $identifiers = [];
        if ($request->has('identifier')) {
            $identifiers = $request->get('identifier');
        }
        if (count($identifiers)>0) {
            $advice_step->stepIdentifiers()->sync($identifiers);
        } else {
            $advice_step->stepIdentifiers()->sync([]);
        }

        $advice_to_advice_step->advices_step_id = $advice_step->id;
        if ($request->has('advice_id')) {
            $advice_to_advice_step->advices_id = $request->get('advice_id');
        }

        if ($request->has('period_id')) {
            $allAdviceToAdviceStep = AdviceToAdviceStep::where('advices_step_id', $advice_to_advice_step->advices_step_id)->where('advices_id', $advice_to_advice_step->advices_id)->whereNotIn('period_id', $request->get('period_id'))->delete();
            foreach ($request->get('period_id') as $period) {
                $current = AdviceToAdviceStep::where('advices_step_id', $advice_to_advice_step->advices_step_id)->where('advices_id', $advice_to_advice_step->advices_id)->where('period_id', $period)->get();
                if (empty($current) || (!empty($current) && $current->count()<=0)) {
                    $insertStepToAdvicePeriod = new AdviceToAdviceStep();
                    $insertStepToAdvicePeriod->advices_step_id = $advice_to_advice_step->advices_step_id;
                    $insertStepToAdvicePeriod->advices_id = $advice_to_advice_step->advices_id;
                    $insertStepToAdvicePeriod->period_id = $period;
                    $insertStepToAdvicePeriod->save();
                }
            }
        }

        foreach (array_keys($request->get('titles')) as $language_id) {
            $advice_step_detail = AdviceStepdetail::firstOrNew(['advices_step_id' => $advice_step->id, 'language_id' => $language_id]);

            if ($request->get('titles')[$language_id] || $request->get('descriptions')[$language_id]) {
                $advice_step_detail->title = $request->get('titles')[$language_id];
                $advice_step_detail->description = $request->get('descriptions')[$language_id];
                $advice_step_detail->save();
            } else {
                $advice_step_detail->delete();
            }
        }

        return $advice_to_advice_step;
    }
}
