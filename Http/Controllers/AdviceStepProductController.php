<?php

namespace Dcms\Advices\Http\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Dcms\Advices\Models\Advice;
use Dcms\Advices\Models\AdviceStep;
use Dcms\Advices\Models\AdviceStepdetail;
use Dcms\Advices\Models\AdviceStepPeriod;
use Dcms\Advices\Models\AdviceStepProduct;
use Dcms\Advices\Models\AdviceStepProductdetail;
use Dcms\Advices\Models\AdviceToAdviceStep;
use Dcms\Advices\Models\ProductsInformation;
use Dcms\Core\Models\Languages\Language;
use Illuminate\Http\Request;
use View;
use Input;
use Session;
use Validator;
use Redirect;
use DB;
use DataTables;
use Auth;
use Form;
use DateTime;

class AdviceStepProductController extends Controller
{
    public function getDatatable($advice_step_id)
    {
        $query = DB::connection('project')
        ->table('products_information_group_to_advices_step')
        ->select(
            'products_information_group_to_advices_step.information_group_id',
            'products_information_group_to_advices_step.id',
            'products_information.title as products_information',
            'products_information.catalogue',
            (DB::connection("project")->raw('Concat("<img src=\'/packages/Dcms/Core/images/flag-",lcase(country),".png\' >") as country'))
        )
        ->join('products_information', 'products_information.information_group_id', '=', 'products_information_group_to_advices_step.information_group_id')
        ->leftJoin('languages', 'products_information.language_id', '=', 'languages.id')
        ->where('products_information_group_to_advices_step.advices_step_id', '=', $advice_step_id);
        
        if (intval(session('overrule_default_by_language_id')) > 0) {
            $query->where('languages.id', session('overrule_default_by_language_id'));
        }

        return Datatables::queryBuilder($query)
            ->addColumn('edit', function ($model) {
                return '<form method="POST" action="/admin/advice_step_products/' . $model->id . '" accept-charset="UTF-8" class="pull-right"> <input name="_token" type="hidden" value="' . csrf_token() . '"> <input name="_method" type="hidden" value="DELETE">
																			<a class="btn btn-xs btn-default" href="/admin/advice_step_products/' . $model->id . '/edit"><i class="far fa-pencil-alt"></i></a>
																			<button class="btn btn-xs btn-default" type="submit" value="Delete this product" onclick="if(!confirm(\'Are you sure to delete this item?\')){return false;};"><i class="far fa-trash-alt"></i></button>
																		</form>';
            })
            ->rawColumns(['country', 'edit'])
            ->make(true);
    }

    public function getProductDatatable($advice_step_id = 0)
    {
        $queryA = DB::connection('project')
        ->table('products_information')
        ->select(
            DB::connection('project')->raw('case when (select count(*)  from products_information_group_to_advices_step where products_information_group_to_advices_step.id = "'.$advice_step_id.'" and products_information_group_to_advices_step.information_group_id = products_information.information_group_id) > 0 then 1 else 0 end as checked'),
            'information_group_id',
            'title',
            'country',
            'catalogue'
        )
        ->leftJoin('languages', 'products_information.language_id', '=', 'languages.id')
        ->orderBy('checked', 'desc');

        $queryB = clone $queryA;
        $queryB->whereRaw('case when (select count(*)  from products_information_group_to_advices_step where products_information_group_to_advices_step.id = "'.$advice_step_id.'" and products_information_group_to_advices_step.information_group_id = products_information.information_group_id) > 0 then 1 else 0 end = 1');

        if (intval(session('overrule_default_by_language_id')) > 0) {
            $queryA->where('products_information.language_id', session('overrule_default_by_language_id'));
        }
        
        return Datatables::queryBuilder($queryB->union($queryA)->orderBy('checked', 'desc'))
        ->addColumn('radio', function ($model) {
            return '<input type="checkbox" name="information_group_id[]" value="'.$model->information_group_id.'" '.($model->checked == 1?'checked="checked"':'').' id="chkbox_'.$model->information_group_id.'" > ';
        })
        ->addColumn('language', function ($model) {
            return '<img src="/packages/Dcms/Core/images/flag-'.strtolower($model->country).'.png" alt="">';
        })
            ->rawColumns(['radio','language'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $advice_step_product = new AdviceStepProduct();
        $information_groups = ProductsInformation::groupBy('information_group_id')->orderBy('title')->get();

        $languages = Language::whereIn('id', [1, 2, 3, 6, 7])->get();

        return View::make('dcms::products/form')
            ->with('advice_step_product', $advice_step_product)
            ->with('languages', $languages)
            ->with('information_groups', $information_groups)
            ->with('advice_to_advice_step_id', $request->get('advice_to_advice_step_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $advice_step_product = AdviceStepProduct::findOrFail($id);
        $information_group_id = $advice_step_product->information_group_id;
        $information_groups = ProductsInformation::groupBy('information_group_id')->orderBy('title')->get();

        $languages = Language::whereIn('id', [1, 2, 3, 6, 7])->get();

        return View::make('dcms::products/form')
            ->with('advice_step_product', $advice_step_product)
            ->with('languages', $languages)
            ->with('information_group_id', $information_group_id)
            ->with('information_groups', $information_groups);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if ($this->validateProductForm() === true) {
            $advice_step_product = new AdviceStepProduct();

            $this->processProductForm($advice_step_product, $request);

            // redirect
            Session::flash('message', 'Successfully created product!');

            return redirect()->route('admin.advice_steps.edit', [$request->get('advice_to_advice_step_id')]);
        } else {
            return $this->validateAdviceStepForm();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        if ($this->validateProductForm() === true) {
            $advice_step_product = AdviceStepProduct::findOrFail($id);

            $this->processProductForm($advice_step_product, $request);

            // redirect
            Session::flash('message', 'Successfully updated product!');

            return redirect()->route('admin.advice_steps.edit', [$advice_step_product->adviceStep->advices()->first()->pivot->id]);
        } else {
            return $this->validateProductForm();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $advice_step_product = AdviceStepProduct::findOrFail($id);
        $advice_step_product->detail()->delete();
        $advice_step_product->delete();

        return redirect()->back();
    }

    private function validateProductForm()
    {
        $rules = [
            //	'title' => 'required'
        ];
        $validator = Validator::make(request()->all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {
            return true;
        }
    }

    private function processProductForm($advice_step_product, $request)
    {
        $v = array_values($request->get('information_group_id'));
        if (count($v)>0) {
            $information_group_id = $v[0];
        }

        $advice_step_product->doses_func = $request->get('doses_func');
        $advice_step_product->doses_func_client = $request->get('doses_func_client');
        $advice_step_product->doses = trim($request->get('doses'));
        $advice_step_product->doses_compute = ((intval($request->get('doses_compute'))>0 && !empty(trim($request->get('doses'))))?1:0);
        $advice_step_product->alternative = ((intval($request->get('alternative'))>0)?1:0);
        $advice_step_product->information_group_id = intval($information_group_id);
        $advice_step_product->save();

        if ($request->has('advice_to_advice_step_id')) {
            $advice_to_advice_step = AdviceToAdviceStep::findOrFail($request->get('advice_to_advice_step_id'));
            $advice_step_product->advices_step_id = $advice_to_advice_step->advices_step_id;
            $advice_step_product->save();
        }

        foreach (array_keys($request->get('descriptions')) as $language_id) {
            $advice_step_product_detail = AdviceStepProductdetail::firstOrNew(['products_information_group_to_advices_step_id' => $advice_step_product->id, 'language_id' => $language_id]);

            if ($request->get('descriptions')[$language_id]) {
                $advice_step_product_detail->description = $request->get('descriptions')[$language_id];
                $advice_step_product_detail->save();
            } else {
                $advice_step_product_detail->delete();
            }
        }
    }
}
