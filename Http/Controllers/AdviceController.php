<?php

namespace Dcms\Advices\Http\Controllers;

use App\Http\Controllers\Controller;
use Dcms\Advices\Http\Controllers\AdviceStepController;
use Dcms\Advices\Models\Advice;
use Dcms\Advices\Models\Condition;
use Dcms\Advices\Models\Plant;
use Illuminate\Http\Request;
use View;
use Input;
use Session;
use Validator;
use Redirect;
use DB;
use DataTables;
use Auth;
use Form;
use DateTime;
use Response;

class AdviceController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:advices-browse')->only('index');
        $this->middleware('permission:advices-add')->only(['create', 'store']);
        $this->middleware('permission:advices-edit')->only(['edit', 'update']);
        $this->middleware('permission:advices-delete')->only('destroy');
    }
    
    public function copy($id)
    {
        $oldAdvice = Advice::find($id);
        $newAdvcice = $oldAdvice->replicate();
        $newAdvcice->save();

        foreach ($oldAdvice->adviceSteps()->get()->unique('id') as $adviceStep) {
            AdviceStepController::copy($adviceStep->id, $newAdvcice->id, $oldAdvice->id);
        }

        //reset relations on EXISTING MODEL (this way you can control which ones will be loaded
        $oldAdvice->relations = ['plants','conditions'];
        
        //load relations on EXISTING MODEL
        $oldAdvice->load('plants', 'conditions');
 
        //re-sync everything
        foreach ($oldAdvice->relations as $relationName => $values) {
            $newAdvcice->{$values}()->sync($oldAdvice->$values()->get());
        }

        // redirect
        Session::flash('message', 'Successfully created advice ('.$newAdvcice->id.')!');
        return Redirect::to('admin/advices');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $plants = DB::connection('project')
            ->table('vw_garden_advice')
            ->select(
                'plant_common',
                'language_id'
            )
            ->orderBy('plant_common')
            ->groupBy('plant_common', 'language_id')
            ->get();

        $conditions = DB::connection('project')
                ->table('vw_garden_advice')
                ->select(
                    'condition_title',
                    'language_id'
                )
                ->orderBy('condition_title')
                ->groupBy('condition_title', 'language_id')
                ->get();

        // load the view
        return View::make('dcms::advices/index')->with('plants', $plants)->with('conditions', $conditions);
    }

    public function getDatatable()
    {
        $query = DB::connection('project')->table('advices')
            ->select(
                'advices.id',
                'advices.pro',
                'advices.nursing',
                'advices.planting',
                'advices.seed',
                'advices.seedling',
                'advices.outdoor',
                'advices.indoor',
                'advices.greenhouse',
                'advices.in_ground',
                'advices.pot',
                'advices.container',
                'advices.raisedbed',
                'conditions_language.condition as condition',
                'plants_language.common as plant',
                (DB::connection("project")->raw('Concat("<img src=\'/packages/Dcms/Core/images/flag-",lcase(country),".svg\'  style=\'width:16px; height:auto;\'>") as country'))
            )
            ->leftJoin('plants_to_advices', 'advices.id', '=', 'plants_to_advices.advices_id')
            ->leftJoin('plants', 'plants.id', '=', 'plants_to_advices.plants_id')
            ->leftJoin('plants_language', 'plants.id', '=', 'plants_language.plant_id')
            ->leftJoin('languages', function ($join) {
                $join->on('plants_language.language_id', '=', 'languages.id');
                $join->on('languages.id', '=', 'plants_language.language_id');
            })
            ->leftJoin('conditions_to_advices', 'advices.id', '=', 'conditions_to_advices.advices_id')
            ->leftJoin('conditions', 'conditions.id', '=', 'conditions_to_advices.conditions_id')
            ->leftJoin('conditions_language', function ($join) {
                $join->on('conditions.id', '=', 'conditions_language.conditions_id');
                $join->on('languages.id', '=', 'conditions_language.language_id');
            })->whereRaw(
                " exists(
                    select * from advices_to_advices_step where  advices_id  = `advices`.`id` and exists (select * from advices_step_language where language_id = `plants_language`.`language_id` and advices_step_language.advices_step_id = advices_to_advices_step.advices_step_id)
                ) "
            );

        if (intval(session('overrule_default_by_language_id')) > 0) {
            $query->where('languages.id', session('overrule_default_by_language_id'));
        }

        if (Session::has('advicefilter')) {
            $filter = Session::get('advicefilter');

            foreach ($filter as $column => $colvalues) {
                if ($column == 'language_id') {
                    foreach ($colvalues as $colvalue) {
                        $query->Where('languages.id', '=', $colvalue);
                    }
                }
                
                if ($column == 'filter') {
                    foreach ($colvalues as $colname => $colvalue) {
                        $query->Where('advices.'.$colname, '=', 1);
                    }
                }
                
                if ($column == 'plant') {
                    foreach ($colvalues as $plantname) {
                        if (!is_null($plantname) && $plantname != 'null') {
                            $query->Where('plants_language.common', '=', $plantname);
                        }
                    }
                }
                
                if ($column == 'condition') {
                    foreach ($colvalues as $conditionname) {
                        if (!is_null($conditionname) && $conditionname != 'null') {
                            $query->Where('conditions_language.condition', '=', $conditionname);
                        }
                    }
                }
            }
        }

        return DataTables::queryBuilder(
            $query
        )
            ->addColumn('edit', function ($model) {
                $edit = '<form method="POST" action="/admin/advices/' . $model->id . '" accept-charset="UTF-8" class="pull-right">
								<input name="_token" type="hidden" value="' . csrf_token() . '">
								<input name="_method" type="hidden" value="DELETE">';
                if (Auth::user()->can('advices-add')) {
                    $edit .= '<a class="btn btn-xs btn-default" href="/admin/advices/' . $model->id . '/copy"><i class="far fa-copy"></i></a>';
                }
                if (Auth::user()->can('advices-browse')) {
                    $edit .= '<a class="btn btn-xs btn-default" href="/admin/advices/' . $model->id . '/preview"><i class="far fa-eye"></i></a>';
                }
                if (Auth::user()->can('advices-edit')) {
                    $edit .= '<a class="btn btn-xs btn-default" href="/admin/advices/' . $model->id . '/edit"><i class="far fa-pencil-alt"></i></a>';
                }
                if (Auth::user()->can('advices-delete')) {
                    //$edit .= '<button class="btn btn-xs btn-default" type="submit" value="Delete this advice" onclick="if(!confirm(\'Are you sure to delete this item?\')){return false;};"><i class="far fa-trash-alt"></i></button>';
                }

                $edit .= '</form>';
                return $edit;
            })
            ->rawColumns(['country', 'edit'])
            ->make(true);
    }

    public function getPlantsDatatable($advice_id = 0)
    {
        $queryA = DB::connection('project')
        ->table('plants_language as x')
        ->select(
            (
                DB::connection("project")->raw('
                case when (select count(*) from  plants_to_advices where plants_to_advices.advices_id = "'.$advice_id.'" and plants_to_advices.plants_id = x.plant_id  ) > 0 then 1 else 0 end as checked,
                plant_id,
                common,
                languages.country')
            )
        )
        ->leftJoin('languages', 'x.language_id', '=', 'languages.id')
        ->orderBy('checked', 'DESC');
        
        $queryB = clone $queryA;
        $queryB->whereRaw('case when (select count(*) from  plants_to_advices where plants_to_advices.advices_id = "'.$advice_id.'" and plants_to_advices.plants_id = x.plant_id  ) > 0 then 1 else 0 end = 1');
     
        if (intval(session('overrule_default_by_language_id')) > 0) {
            $queryA->where('x.language_id', session('overrule_default_by_language_id'));
        }

        return Datatables::queryBuilder($queryB->union($queryA)->orderBy('checked', 'desc'))
                ->addColumn('radio', function ($model) {
                    return '<input type="checkbox" name="plants[]" value="'.$model->plant_id.'" '.($model->checked == 1?'checked="checked"':'').' id="chkbox_'.$model->plant_id.'" > ';
                })
                ->addColumn('language', function ($model) {
                    return '<img src="/packages/Dcms/Core/images/flag-'.strtolower($model->country).'.svg" style="width:16px; height: auto;" alt="">';
                })
                ->rawColumns(['radio','language'])
                ->make(true);
    }
    
    public function getConditionsDatatable($advice_id = 0)
    {
        $queryA = DB::connection('project')
        ->table('conditions_language as x')
        ->select(
            (
                DB::connection("project")->raw('
                case when (select count(*) from conditions_to_advices where conditions_to_advices.advices_id = "'.$advice_id.'" and conditions_to_advices.conditions_id = x.conditions_id) > 0 then 1 else 0 end as checked,
                conditions_id,
                `condition`,
                languages.country')
            )
        )
        ->leftJoin('languages', 'x.language_id', '=', 'languages.id')
        ->orderBy('checked', 'DESC');

        $queryB = clone $queryA;
        $queryB->whereRaw('case when (select count(*) from conditions_to_advices where conditions_to_advices.advices_id = "'.$advice_id.'" and conditions_to_advices.conditions_id = x.conditions_id) > 0 then 1 else 0 end = 1');
        
        if (intval(session('overrule_default_by_language_id')) > 0) {
            $queryA->where('x.language_id', session('overrule_default_by_language_id'));
        }
    
        return Datatables::queryBuilder($queryB->union($queryA)->orderBy('checked', 'desc'))
                ->addColumn('radio', function ($model) {
                    return '<input type="checkbox" name="conditions[]" value="'.$model->conditions_id.'" '.($model->checked == 1?'checked="checked"':'').' id="chkbox_'.$model->conditions_id.'" > ';
                })
                ->addColumn('language', function ($model) {
                    return '<img src="/packages/Dcms/Core/images/flag-'.strtolower($model->country).'.svg" style="width:16px; height: auto;" alt="">';
                })
                ->rawColumns(['radio','language'])
                ->make(true);
    }

    public function getExtendedModel()
    {
        //do nothing let the extend class hook into this
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $plants = Plant::with('plantdetail')->get();
        $conditions = Condition::with('detail')->get();
        $advice_steps = [];

        // load the create form (app/views/advices/create.blade.php)
        return View::make('dcms::advices/form')
            ->with('advice', new Advice())
            ->with('plants', $plants)
            ->with('conditions', $conditions)
            ->with('advice_steps', $advice_steps);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $advice = Advice::findOrFail($id);
        $plants = Plant::with('plantdetail')->get();
        $conditions = Condition::with('detail')->get();
        $advice_steps = $advice->adviceSteps()->with('detail')->get();

        return View::make('dcms::advices/form')
            ->with('advice', $advice)
            ->with('plants', $plants)
            ->with('conditions', $conditions)
            ->with('advice_steps', $advice_steps);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if ($this->validateAdviceForm() === true) {
            $advice = new Advice();

            $advice = $this->processAdviceForm($advice, $request);

            // redirect
            Session::flash('message', 'Successfully created advice!');

            if ($request->has('create-step')) {
                return redirect()->route('admin.advice_steps.create', ['advice_id' => $advice->id]);
            } else {
                return Redirect::to('admin/advices');
            }
        } else {
            return $this->validateAdviceForm();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        if ($this->validateAdviceForm() === true) {
            $advice = Advice::findOrFail($id);

            $advice = $this->processAdviceForm($advice, $request);

            // redirect
            Session::flash('message', 'Successfully updated advice!');

            if ($request->has('create-step')) {
                return redirect()->route('admin.advice_steps.create', ['advice_id' => $advice->id]);
            } else {
                return Redirect::to('admin/advices');
            }
        } else {
            return $this->validateAdviceForm();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $advice = Advice::findOrFail($id);
        $advice->adviceSteps()->sync([]);
        $advice->delete();

        /*DB::connection('dcm')->select(DB::raw('
                    #clean up advices_to_advices_step  via LARAVEL (relation sync())

                    #clean up steps
                    delete from advices_step where id not in (select advices_step_id from advices_to_advices_step );

                    ##clean up advices_step_language (via FK)

                    #clean up step alert
                    delete from advices_step_alert where id not in (select advices_step_alert_id from advices_step_to_advices_step_alert);
                    ## clean up advices_step_alert_language (via FK)

                    #clean up step (alert) category
                    delete from advices_step_category where id not in (select category_id from advices_step_alert);

                    #clean up parameters
                    delete from advices_step_parameter where id not in (select parameter_id from advices_step_alert) AND id not in (select parameter_id from advices_step);

                    # clean up products_information_group_to_advices_step (via FK)
                    # clean up products_information_group_to_advices_step_language (via FK)'))->get();*/

        DB::connection('project')->select(DB::raw('delete from advices_step where id not in (select advices_step_id from advices_to_advices_step )'));
        DB::connection('project')->select(DB::raw('delete from advices_step_alert where id not in (select advices_step_alert_id from advices_step_to_advices_step_alert)'));
        DB::connection('project')->select(DB::raw('delete from advices_step_category where id not in (select category_id from advices_step_alert)'));
        DB::connection('project')->select(DB::raw('delete from advices_step_parameter where id not in (select parameter_id from advices_step_alert) AND id not in (select parameter_id from advices_step)'));

        Session::flash('message', 'Successfully deleted the advice!');

        return Redirect::to('admin/advices');
    }

    private function validateAdviceForm()
    {
        $rules = [
//            'plants' => 'required'
        ];
        $validator = Validator::make(request()->all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {
            return true;
        }
    }

    private function processAdviceForm($advice, $request)
    {
        $advice->pro = $request->has('pro');
        $advice->nursing = $request->has('nursing');
        $advice->planting = $request->has('planting');
        $advice->seed = $request->has('seed');
        $advice->seedling = $request->has('seedling');
        $advice->outdoor = $request->has('outdoor');
        $advice->indoor = $request->has('indoor');
        $advice->greenhouse = $request->has('greenhouse');
        $advice->in_ground = $request->has('in_ground');
        $advice->pot = $request->has('pot');
        $advice->container = $request->has('container');
        $advice->raisedbed = $request->has('raisedbed');
        $advice->save();

        if ($request->has('plants')) {
            $advice->plants()->sync(array_values($request->get('plants')));
        } else {
            $advice->plants()->sync([]);
        }

        if ($request->has('conditions')) {
            $advice->conditions()->sync(array_values($request->get('conditions')));
        } else {
            $advice->conditions()->sync([]);
        }

        return $advice;
    }

    public function preview(Request $request, $id = null)
    {
        $A = Advice::with('plants')
                        ->with('plants.plantdetail')
                        ->with(['conditions','conditions.detail'])
                        ->with(['adviceSteps', 'adviceSteps.detail']);

        if (!is_null($id) && intval($id)>0) {
            $A->where('id', '=', intval($id));
        }
        return  Response::json($A->get());
    }
}
