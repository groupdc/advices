<?php

Route::group(['middleware' => ['web']], function () {
    Route::group(array("prefix" => "admin", "as"=>"admin."), function () {
        Route::group(['middleware' => 'auth:dcms'], function () {

            //Advices
            Route::group(array("prefix" => "avices" ,"as"=>"advices."), function () {
                Route::get('{id}/copy', array('as'=>'copy', 'uses' => 'AdviceController@copy'));

                //API
                Route::group(array("prefix" => "api", "as"=>"api."), function () {
                    Route::group(array("prefix" => "table", "as"=>"table."), function () {
                        Route::any('conditions/{advice_id?}', array('as'=>'conditions', 'uses' => 'AdviceController@getConditionsDatatable'));
                        Route::any('plants/{advice_id?}', array('as'=>'plants', 'uses' => 'AdviceController@getPlantsDatatable'));
                    });
                    Route::any('table', array('as'=>'table', 'uses' => 'AdviceController@getDatatable'));
                    Route::any('relation/table/{advice_id?}', array('as'=>'relation.table', 'uses' => 'AdviceController@getRelationDatatable'));
                });
            });

            //Advice steps
            Route::group(array("prefix" => "advice_steps" ,"as"=>"advice_steps."), function () {
                //API
                Route::group(array("prefix" => "api", "as"=>"api."), function () {
                    Route::any('table/{advice_id}', array('as'=>'table', 'uses' => 'AdviceStepController@getDatatable'));
                });
            });

            //Advice step products
            Route::group(array("prefix" => "advice_step_products" ,"as"=>"advice_step_products."), function () {
                //API
                Route::group(array("prefix" => "api", "as"=>"api."), function () {
                    Route::any('table/products/{advice_step_id?}', array('as'=>'table.products', 'uses' => 'AdviceStepProductController@getProductDatatable'));
                    Route::any('table/{advice_step_id}', array('as'=>'table', 'uses' => 'AdviceStepProductController@getDatatable'));
                });
            });

            Route::get('advices/{id}/preview/', array('as'=>'advices.preview', 'uses' => 'AdviceController@preview'));
            Route::get('advices/{id}/copy', ['as'=>'advices.copy','uses'=>'AdviceController@copy']);

            Route::get('advice_steps/{id}/copy', ['as'=>'advices_step.copy','uses'=>'AdviceStepController@copy']);

            Route::resource('advices', 'AdviceController');
            Route::resource('advice_steps', 'AdviceStepController');
            Route::resource('advice_step_products', 'AdviceStepProductController');
        });
    });
});
