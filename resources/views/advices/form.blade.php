@extends("dcms::template/layout")

@section("content")

    <div class="main-header">
        <h1>Advices</h1>
        <ol class="breadcrumb">
            <li><a href="{!! URL::to('admin/dashboard') !!}"><i class="far fa-tachometer-alt-average"></i> Dashboard</a></li>
            <li><a href="{!! URL::to('admin/advices') !!}"><i class="far fa-graduation-cap"></i> Advices</a></li>
            @if($advice->exists)
                <li class="active"><i class="far fa-pencil"></i> Advice {{$advice->id}}</li>
            @else
                <li class="active"><i class="far fa-plus-circle"></i> Create advice</li>
            @endif
        </ol>
    </div>

    <div class="main-content">
        @if($advice->exists)
            <form action="{{ route('admin.advices.update', $advice->id) }}" method="post" onsubmit="return resetTables();">
            {{ method_field('PUT') }}
        @else
            <form action="{{ route('admin.advices.store') }}" method="post" onsubmit="return resetTables();">
        @endif
        {!! csrf_field() !!}
        <div class="row">
            <div class="col-md-9">
                <div class="main-content-tab tab-container">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#steps" role="tab" data-toggle="tab">Steps</a></li>
                        <li><a href="#conditions" role="tab" data-toggle="tab">Conditions</a></li>
                        <li><a href="#plants" role="tab" data-toggle="tab">Plants</a></li>
                        @yield('extratabs')
                    </ul>

                    <div class="tab-content">
                    <!-- Steps -->
                        <div id="steps" class="tab-pane active">
                            <div class="tab-content">
                                <div class="bntbar btnbar-right">
                                    <button class="btn btn-small btn-primary" name="create-step" value="create">Create new</button>
                                </div>
                                <h2>Overview</h2>
                                <table id="datatable" class="table table-hover table-condensed" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Step</th>
                                        <th>Period</th>
                                        <th>Sort</th>
                                        <th>Once</th>
                                        <th>Reject</th>
                                        <th>Product</th>
                                        <th>Country</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>

                        <!-- Conditions -->
                        <div id="conditions" class="tab-pane">
                            <div class="tab-content">

                                <h2>Overview</h2>
                                <table id="datatable_conditions" class="table table-hover table-condensed" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>ID</th>
                                        <th>Condition</th>
                                        <th>Country</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>

                        <!-- Plants -->
                        <div id="plants" class="tab-pane">
                            <div class="tab-content">
                                
                                <h2>Overview</h2>
                                <table id="datatable_plants" class="table table-hover table-condensed" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>ID</th>
                                        <th>Plant</th>
                                        <th>Country</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>

                        

                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="main-content-block">
                    <div class="form-group">
                        <input type="checkbox" name="pro" id="pro" class="form-checkbox" {{ $advice->pro ? 'checked' : '' }}>
                        <label for="pro" class="checkbox {{ $advice->pro ? 'active' : '' }}">PRO (Landscaper)</label>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="nursing" id="nursing" class="form-checkbox" {{ $advice->nursing ? 'checked' : '' }}>
                        <label for="nursing" class="checkbox {{ $advice->nursing ? 'active' : '' }}">Nursing</label>
                    </div>

                    <div class="form-group">
                        <input type="checkbox" name="planting" id="planting" class="form-checkbox" {{ $advice->planting ? 'checked' : '' }}>
                        <label for="planting" class="checkbox {{ $advice->planting ? 'active' : '' }}">Planting</label>
                    </div>

                    <div class="form-group">
                        <input type="checkbox" name="seed" id="seed" class="form-checkbox" {{ $advice->seed ? 'checked' : '' }}>
                        <label for="seed" class="checkbox {{ $advice->seed ? 'active' : '' }}">Seed</label>
                    </div>

                    <div class="form-group">
                        <input type="checkbox" name="seedling" id="seedling" class="form-checkbox" {{ $advice->seedling ? 'checked' : '' }}>
                        <label for="seedling" class="checkbox {{ $advice->seedling ? 'active' : '' }}">Seedling</label>
                    </div>

                    <div class="form-group">
                        <input type="checkbox" name="outdoor" id="outdoor" class="form-checkbox" {{ $advice->outdoor ? 'checked' : '' }}>
                        <label for="outdoor" class="checkbox {{ $advice->outdoor ? 'active' : '' }}">Outdoor</label>
                    </div>

                    <div class="form-group">
                        <input type="checkbox" name="indoor" id="indoor" class="form-checkbox" {{ $advice->indoor ? 'checked' : '' }}>
                        <label for="indoor" class="checkbox {{ $advice->indoor ? 'active' : '' }}">Indoor</label>
                    </div>

                    <div class="form-group">
                        <input type="checkbox" name="greenhouse" id="greenhouse" class="form-checkbox" {{ $advice->greenhouse ? 'checked' : '' }}>
                        <label for="greenhouse" class="checkbox {{ $advice->greenhouse ? 'active' : '' }}">Greenhouse</label>
                    </div>

                    <div class="form-group">
                        <input type="checkbox" name="in_ground" id="in_ground" class="form-checkbox" {{ $advice->in_ground ? 'checked' : '' }}>
                        <label for="in_ground" class="checkbox {{ $advice->in_ground ? 'active' : '' }}">In Ground</label>
                    </div>

                    <div class="form-group">
                        <input type="checkbox" name="pot" id="pot" class="form-checkbox" {{ $advice->pot ? 'checked' : '' }}>
                        <label for="pot" class="checkbox {{ $advice->pot ? 'active' : '' }}">Pot</label>
                    </div>

                    <div class="form-group">
                        <input type="checkbox" name="container" id="container" class="form-checkbox" {{ $advice->container ? 'checked' : '' }}>
                        <label for="container" class="checkbox {{ $advice->container ? 'active' : '' }}">Container</label>
                    </div>

                    <div class="form-group">
                        <input type="checkbox" name="raisedbed" id="raisedbed" class="form-checkbox" {{ $advice->raisedbed ? 'checked' : '' }}>
                        <label for="raisedbed" class="checkbox {{ $advice->raisedbed ? 'active' : '' }}">Raised bed</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="main-content-block">
                    {!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
                    <a href="{!! URL::previous() !!}" class="btn btn-default">Cancel</a>
                </div>
            </div>
        </div>
        </form>
    </div>

@stop

@section("script")
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.css">

    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.js"></script>

    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/js/bootstrap.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckeditor/ckeditor.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckeditor/adapters/jquery.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core//ckfinder/ckfinder.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core//ckfinder/ckbrowser.js') !!}"></script>

    <script type="text/javascript">
        conditionTable = null;
        plantsTable = null;
        function resetTables(){
            conditionTable.search('').draw();
            plantsTable.search('').draw();
            return true;
        }

        $(document).ready(function () {
            //CKFinder for CKEditor
            CKFinder.setupCKEditor(null, '/packages/Dcms/Core/ckfinder/');

            //CKEditor
            $("textarea[id='description']").ckeditor();
            $("textarea[id='body']").ckeditor();

            //Bootstrap Tabs
            $(".tab-container .nav-tabs a").click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });

            //Datatable
            datatable = $('#datatable').DataTable({
                "ordering": false,
                "paging": false,
                "processing": true,
                "ajax": "{{ route('admin.advice_steps.api.table', [ $advice->id ?: 0]) }}",
                "columns": [
                    {data: 'advices_step_id', name: 'advices_step_id'},
                    {data: 'advice_step', name: 'advices_step_language.title'},
                    {data: 'period_id', name: 'period', searchable: false},
                    {data: 'sort_id', name: 'sort', searchable: false},
                    {data: 'once', name: 'once', searchable: false},
                    {data: 'reject', name: 'reject', searchable: false},
                    {data: 'products', name: 'product', searchable: false},
                    {data: 'country', name: 'country', searchable: false},
                    {data: 'edit', name: 'edit', orderable: false, searchable: false}
                ]
            });

            //Datatable
            
            conditionTable = $('#datatable_conditions').DataTable({
                "paging": false,
                "processing": true,
                "ajax": "{{ route('admin.advices.api.table.conditions', ($advice->id ?: 0)) }}",
                "columns": [
                    {data: 'radio', name: 'radio'},
                    {data: 'conditions_id', name: 'conditions_id'},
                    {data: 'condition', name: 'x.condition'},
                    {data: 'language', name: 'language'}
                ],
                "createdRow": function( row, data, cells ) {
                    if (~data['radio'].indexOf('checked')) {   
                    $(row).addClass('selected');
                    }
                },
            });

            //Datatable
            
            plantsTable = $('#datatable_plants').DataTable({
                "paging": false,
                "processing": true,
                "ajax": "{{ route('admin.advices.api.table.plants', ($advice->id ?: 0)) }}",
                "columns": [
                    {data: 'radio', name: 'radio'},
                    {data: 'plant_id', name: 'plant_id'},
                    {data: 'common', name: 'common'},
                    {data: 'language', name: 'language'}
                ],
                "createdRow": function( row, data, cells ) {
                    if (~data['radio'].indexOf('checked')) {   
                    $(row).addClass('selected');
                    }
                }
            });
            
            $('#datatable_conditions').on('click', 'tbody tr', function (e) {
                if($(e.target).is('input[type=checkbox]')) {
                    $(this).toggleClass('selected');
                } else {
                    var clicked = $(this).find('input:checkbox');
                    if ( clicked.is(':checked')) {
                        clicked.prop('checked', false);
                    } else {
                        clicked.prop('checked', true);
                    }
        
                    $(this).toggleClass('selected');
                }
            });
            
            $('#datatable_plants').on('click', 'tbody tr', function (e) {
                if($(e.target).is('input[type=checkbox]')) {
                    $(this).toggleClass('selected');
                } else {
                    var clicked = $(this).find('input:checkbox');
                    if ( clicked.is(':checked')) {
                        clicked.prop('checked', false);
                    } else {
                        clicked.prop('checked', true);
                    }
        
                    $(this).toggleClass('selected');
                }
            });
        });

    </script>

@stop
