@extends("dcms::template/layout")

@section("content")

    <div class="main-header">
        <h1>Advices</h1>
        <ol class="breadcrumb">
            <li><a href="{!! URL::to('admin/dashboard') !!}"><i class="far fa-tachometer-alt-average"></i> Dashboard</a></li>
            <li class="active"><i class="far fa-graduation-cap"></i> Advices</li>
        </ol>
    </div>

    <div class="main-content">
        <div class="row">
            <div class="col-md-12">
                <div class="main-content-block">

                    @if (Session::has('message'))
                        <div class="alert alert-info">{!! Session::get('message') !!}</div>
                    @endif

                    @can('advices-add')
                    <div class="btnbar btnbar-right"><a class="btn btn-small btn-primary" href="{!! URL::to('/admin/advices/create') !!}">Create new</a></div>
                    @endcan

                    <h2>Overview</h2>

                    
                    <div class="panel filter up">
                        <div class="panel-heading">
                            <h3>Filter <i class="far fa-plus-square right"></i></h3>
                        </div>
                        <div class="panel-body" @if(Session::has('advicefilter')) style="display:block;" @endif>
                            <form method="GET" action="/admin/advices" accept-charset="UTF-8">

                                <?php
                                if (Request::has('resetfilters') && Request::get('resetfilters') == 'true') {
                                    Session::forget('advicefilter');
                                }
                                ?>

                                @if(Request::has('advicefilter') || Session::has('advicefilter'))
                                    @if(Request::has('advicefilter'))
                                        <?php
                                        Session::put('advicefilter', Request::get('advicefilter')); //put in a session so the Datatable request can accesss the variables
                                        ?>
                                    @endif
                                @endif

                                <h4>Country</h4>
                                <ul>
                                    <li style="display:inline;"><label for="be"><input class="changeLanguage" type="checkbox" name="advicefilter[language_id][1]" id="be" value="1"
                                                                                       @if(Session::has('advicefilter.language_id') && Session::has('advicefilter.language_id.1')) checked @endif >
                                            <img src="/packages/Dcms/Core/images/flag-be.svg" style="width:16px; height: auto;"></label></li>
                                    <li style="display:inline; margin-left:15px;"><label for="nl"><input class="changeLanguage" type="checkbox" name="advicefilter[language_id][3]"
                                                                                                         id="nl" value="3"
                                                                                                         @if(Session::has('advicefilter.language_id') && Session::has('advicefilter.language_id.3')) checked @endif>
                                            <img src="/packages/Dcms/Core/images/flag-nl.svg" style="width:16px; height: auto;"></label></li>
                                    <li style="display:inline; margin-left:15px;"><label for="fr"><input class="changeLanguage" type="checkbox" name="advicefilter[language_id][6]"
                                                                                                         id="fr" value="6"
                                                                                                         @if(Session::has('advicefilter.language_id') && Session::has('advicefilter.language_id.6')) checked @endif>
                                            <img src="/packages/Dcms/Core/images/flag-fr.svg" style="width:16px; height: auto;"></label></li>
                                    <li style="display:inline; margin-left:15px;"><label for="de"><input class="changeLanguage" type="checkbox" name="advicefilter[language_id][7]"
                                                                                                         id="de" value="7"
                                                                                                         @if(Session::has('advicefilter.language_id') && Session::has('advicefilter.language_id.7')) checked @endif>
                                            <img src="/packages/Dcms/Core/images/flag-de.svg" style="width:16px; height: auto;"></label></li>
                                </ul>
                                <hr>
                                <h4>Advice</h4>
                                <ul>
                                    <li style="display:inline;"><label for="pro"><input class="" type="checkbox" name="advicefilter[filter][pro]" id="pro"
                                                                                          value="1"
                                                                                          @if(Session::has('advicefilter.filter') && Session::has('advicefilter.filter.pro')) checked @endif>
                                            PRO</label></li>
                                    <li style="display:inline; margin-left:15px;"><label for="nursing"><input class="" type="checkbox" name="advicefilter[filter][nursing]"
                                                                                                          id="nursing" value="1"
                                                                                                          @if(Session::has('advicefilter.filter') && Session::has('advicefilter.filter.nursing')) checked @endif>
                                            Nursing</label></li>
                                    <li style="display:inline; margin-left:15px;"><label for="planting"><input class="" type="checkbox" name="advicefilter[filter][planting]"
                                                                                                          id="planting" value="1"
                                                                                                          @if(Session::has('advicefilter.filter') && Session::has('advicefilter.filter.planting')) checked @endif>
                                            Planting</label></li>
                                    <li style="display:inline; margin-left:15px;"><label for="seed"><input class="" type="checkbox" name="advicefilter[filter][seed]"
                                                                                                          id="seed" value="1"
                                                                                                          @if(Session::has('advicefilter.filter') && Session::has('advicefilter.filter.seed')) checked @endif>
                                            Seed</label></li>
                                    <li style="display:inline; margin-left:15px;"><label for="seedling"><input class="" type="checkbox" name="advicefilter[filter][seedling]"
                                                                                                          id="seedling" value="1"
                                                                                                          @if(Session::has('advicefilter.filter') && Session::has('advicefilter.filter.seedling')) checked @endif>
                                            Seedling</label></li>
                                    <li style="display:inline; margin-left:15px;"><label for="outdoor"><input class="" type="checkbox" name="advicefilter[filter][outdoor]"
                                                                                                          id="outdoor" value="1"
                                                                                                          @if(Session::has('advicefilter.filter') && Session::has('advicefilter.filter.outdoor')) checked @endif>
                                            Outdoor</label></li>
                                    <li style="display:inline; margin-left:15px;"><label for="indoor"><input class="" type="checkbox" name="advicefilter[filter][indoor]"
                                                                                                          id="indoor" value="1"
                                                                                                          @if(Session::has('advicefilter.filter') && Session::has('advicefilter.filter.indoor')) checked @endif>
                                            Indoor</label></li>
                                    <li style="display:inline; margin-left:15px;"><label for="greenhouse"><input class="" type="checkbox" name="advicefilter[filter][greenhouse]"
                                                                                                          id="greenhouse" value="1"
                                                                                                          @if(Session::has('advicefilter.filter') && Session::has('advicefilter.filter.greenhouse')) checked @endif>
                                            Greenhouse</label></li>
                                    <li style="display:inline; margin-left:15px;"><label for="in_ground"><input class="" type="checkbox" name="advicefilter[filter][in_ground]"
                                                                                                          id="in_ground" value="1"
                                                                                                          @if(Session::has('advicefilter.filter') && Session::has('advicefilter.filter.in_ground')) checked @endif>
                                            In_ground</label></li>
                                    <li style="display:inline; margin-left:15px;"><label for="pot"><input class="" type="checkbox" name="advicefilter[filter][pot]"
                                                                                                          id="pot" value="pot"
                                                                                                          @if(Session::has('advicefilter.filter') && Session::has('advicefilter.filter.pot')) checked @endif>
                                            Pot</label></li>
                                    <li style="display:inline; margin-left:15px;"><label for="container"><input class="" type="checkbox" name="advicefilter[filter][container]"
                                                                                                          id="container" value="1"
                                                                                                          @if(Session::has('advicefilter.filter') && Session::has('advicefilter.filter.container')) checked @endif>
                                            Container</label></li>
                                </ul>
                                <hr>
                                <h4>Plant</h4>
                                <select class="form-control" name="advicefilter[plant][]">
                                    <option value="">-all-</option>
                                    <?php
                                    $allreadyprinted = [];
                                    ?>
                                    @foreach($plants as $plant)
                                        <option class='plantdd plantlanguage{{$plant->language_id}}'
                                                value="{{$plant->plant_common}}"
                                                @if(Session::has('advicefilter.plant') && Session::get('advicefilter.plant.0') == $plant->plant_common ) selected @endif>{{$plant->plant_common}}</option>
                                    @endforeach
                                </select>
                                <hr>
                                <h4>condition</h4>
                                <select class="form-control" name="advicefilter[condition][]">
                                    <option value="">-all-</option>
                                    <?php
                                    $allreadyprinted = [];
                                    ?>
                                    @foreach($conditions as $condition)
                                        <option class='conditiondd conditionlanguage{{$condition->language_id}}'
                                                value="{{$condition->condition_title}}"
                                                @if(Session::has('advicefilter.condition') && Session::get('advicefilter.condition.0') == $condition->condition_title ) selected @endif>{{$condition->condition_title}}</option>
                                    @endforeach
                                </select>

                                <div class="btnbar btnbar-left" style="margin-top:15px;">
                                    {!! Form::button('Set Filter', array('class' => 'btn btn-primary', 'type'=>'submit')) !!}
                                    <a class="btn btn-default" href="{{url('admin/advices?resetfilters=true')}}">Reset Filter</a>
                                </div>
                            </form>
                        </div>
                    </div>


        <table id="datatable" class="table table-hover table-condensed" style="width:100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Plant</th>
                    <th>Condition</th>
                    <th>Pro</th>
                    <th>Nursing</th>
                    <th>Planting</th>
                    <th>Seed</th>
                    <th>Seedling</th>
                    <th>Outdoor</th>
                    <th>Indoor</th>
                    <th>Greenhouse</th>
                    <th>in_ground</th>
                    <th>Pot</th>
                    <th>Container</th>
                    <th>Country</th>
                    <th></th>
                </tr>
            </thead>
        </table>

    <script type="text/javascript">
        $(document).ready(function() {
            oTable = $('#datatable').DataTable({
                "pageLength": 50,
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('admin.advices.api.table') }}",
                "columns": [
                    {data: 'id', name: 'id'},
                    {data: 'plant', name: 'plants_language.common'},
                    {data: 'condition', name: 'conditions_language.condition'},
                    {data: 'pro', name: 'advices.pro'},
                    {data: 'nursing', name: 'advices.nursing'},
                    {data: 'planting', name: 'advices.planting'},
                    {data: 'seed', name: 'advices.seed'},
                    {data: 'seedling', name: 'advices.seedling'},
                    {data: 'outdoor', name: 'advices.outdoor'},
                    {data: 'indoor', name: 'advices.indoor'},
                    {data: 'greenhouse', name: 'advices.greenhouse'},
                    {data: 'in_ground', name: 'advices.in_ground'},
                    {data: 'pot', name: 'advices.pot'},
                    {data: 'container', name: 'advices.container'},
                    {data: 'country', name: 'country', searchable: false},
                    {data: 'edit', name: 'edit', orderable: false, searchable: false}
                ]
            });
        });
    </script>

                    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.css">

                    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.min.js"></script>
                    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.js"></script>
                </div>
            </div>
        </div>
    </div>
@stop

@section("script")
    <script type="text/javascript">
        languageFilter = "";
        function filterLanguage() {
            $('.changeLanguage').each(function (index) {
                if ($(this)[0].checked == true) {
                    languageFilter = '.plantlanguage' + $(this)[0].value;
                    $('.plantlanguage' + $(this)[0].value).show();
                    $('.conditionlanguage' + $(this)[0].value).show();
                } else {
                    $('.plantlanguage' + $(this)[0].value).hide();
                    $('.conditionlanguage' + $(this)[0].value).hide();
                }
            });            
        }

        $(document).ready(function () {
            $('.plantdd').hide();
            filterLanguage();

            $('.changeLanguage').change(function () {
                filterLanguage();
            })

            //Filter toggle
            $('.filter .panel-heading').click(function () {
                $(this).closest('.panel').toggleClass('up');
            });

            $("#btn-reset").click(function () {
                var appendDiv = jQuery($(".condition:last")[0].outerHTML);
                $(".condition").remove();
                $("#filters").prepend(appendDiv.attr('id', '0'));
                $("#0").find('select').val('0');
                $("#0").find('input[type=text]').val('');
            });
        });
    </script>
@stop
