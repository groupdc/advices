@extends("dcms::template/layout")

@section("content")

    <div class="main-header">
        <h1>Advice Steps</h1>
        <ol class="breadcrumb">
            <li><a href="{!! URL::to('admin/dashboard') !!}"><i class="far fa-tachometer-alt-average"></i> Dashboard</a></li>
            <li><a href="{!! URL::to('admin/advices') !!}"><i class="far fa-pencil"></i> Advices</a></li>
            @if($advice_step->exists)
                <li><a href="{!! URL::to('admin/advices/'.$advice_to_advice_step->advices_id.'/edit') !!}"><i class="far fa-pencil"></i> Advice:{{$advice_to_advice_step->advices_id}}</a></li>
                <li class="active">Steps</li>
                <li class="active">Edit</li>
            @else
                <li><a href="{!! URL::to('admin/advices/'.$advice_id.'/edit') !!}"><i class="far fa-pencil"></i> Advice:{{$advice_id}}</a></li>
                <li class="active">Steps</li>
                <li class="active">Create</li>
            @endif
        </ol>
    </div>

    <div class="main-content">
        @if($advice_step->exists)
        <form action="{{ route('admin.advice_steps.update', $advice_to_advice_step->id) }}" method="post">
            {{ method_field('PUT') }}
        @else
        <form action="{{ route('admin.advice_steps.store') }}" method="post">
            @if(isset($advice_id))
                <input type="hidden" name="advice_id" value="{{ $advice_id }}">
            @endif
        @endif
        {!! csrf_field() !!}
        <div class="row">
            <div class="col-md-9">
                <div class="main-content-tab tab-container">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#data" role="tab" data-toggle="tab">Data</a></li>
                        <li><a href="#period" role="tab" data-toggle="tab">Period</a></li>
                        <li><a href="#products" role="tab" data-toggle="tab">Products</a></li>
                        <li><a href="#alerts" role="tab" data-toggle="tab">Alerts</a></li>
                        @yield('extratabs')
                    </ul>

                    <div class="tab-content">
                        <!-- Data -->
                        <div id="data" class="tab-pane active">
                            <div class="tab-content">
                                @if(isset($languages))
                                    <ul class="nav nav-tabs" role="tablist">
                                        @foreach($languages as $key => $language)

                                            <li class="{!! (( intval(session('overrule_default_by_language_id')) == intval($language->id)) ? 'active' : ((intval(session('overrule_default_by_language_id')) == 0 && $key == 0) ? 'active': '')) !!}"><a href="{!! '#' . $language->language . '-' . $language->country !!}" role="tab" data-toggle="tab"><img src="{!! asset('/packages/Dcms/Core/images/flag-' . strtolower($language->country) . '.svg') !!}" width="16" height="16" /> {!! $language->language_name !!} </a></li>
                                        @endforeach
                                    </ul>

                                    <div class="tab-content">
                                        @foreach($languages as $key => $information)
                                        
                                            <div id="{!! $information->language . '-' . $information->country !!}" class="tab-pane {!! (( intval(session('overrule_default_by_language_id')) == intval($information->id)) ? 'active' : ( (intval(session('overrule_default_by_language_id')) == 0 && $key == 0) ? 'active': '')) !!}">
                                                <div class="form-group">
                                                    <label for="titles[{{$information->id}}]">Title</label>
                                                    <input type="text" class="form-control" id="titles[{{$information->id}}]" name="titles[{{$information->id}}]" value="{{ $advice_step->detail->where('language_id', $information->id)->first() ? $advice_step->detail->where('language_id', $information->id)->first()->title : '' }}">
                                                </div>

                                                <div class="form-group">
                                                    <label for="descriptions[{{$information->id}}]">Description</label>
                                                    <textarea name="descriptions[{{$information->id}}]" id="descriptions[{{$information->id}}]" rows="10" class="form-control ck-text">{{ $advice_step->detail->where('language_id', $information->id)->first() ? $advice_step->detail->where('language_id', $information->id)->first()->description : '' }}</textarea>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>

                                @endif
                            </div>
                        </div>

                        <!-- Periods -->
                        <div id="period" class="tab-pane">
                            <div class="tab-content">
                               @foreach($periods as $period)
                                    <div class="form-group">
                                        <input type="checkbox" name="period_id[{{$period->id}}]" id="period_{{$period->id}}" value="{{$period->id}}" class="form-checkbox" {{ in_array($period->id,$allActivePeriods) ? 'checked':'' }}>
                                        <label for="period_{{$period->id}}" class="checkbox {{ in_array($period->id,$allActivePeriods) ? 'active':'' }}">{{ ucfirst($period->period) }}</label>
                                    </div>                                        
                               @endforeach
                            </div>
                        </div>

                        <!-- Products -->
                        <div id="products" class="tab-pane">
                            <div class="tab-content">
                                <div class="bntbar btnbar-right">
                                    <button class="btn btn-small btn-primary" name="create-product" value="create">Create new</button>
                                </div>
                                <h2>Overview</h2>
                                <table id="datatable" class="table table-hover table-condensed" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Product</th>
                                        <th>Hobby/Pro</th>
                                        <th>Country</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>

                        <!-- Alert -->
                        <div id="alerts" class="tab-pane">
                            <div class="tab-content">
                                @if(!is_null($adviceStepAlert) && count($adviceStepAlert))
                                    @foreach($adviceStepAlert as $alert)
                                        @if($alert->detail->count()>=1)

                                            <div class="form-group">
                                                <input type="checkbox" name="alert[{{ $alert->id}}]" id="alert[{{ $alert->id}}]" class="form-checkbox" {{ $advice_step->adviceStepsAlert->contains('id',$alert->id) ? 'checked' : '' }} value="{{ $alert->id}}" >
                                                <label for="alert[{{ $alert->id}}]" class="checkbox {{ $advice_step->adviceStepsAlert->contains('id',$alert->id) ? 'active' : '' }}"> {{ $alert->parameter->name }} ({{ $alert->parameter->value }}) </label>
                                                {{$alert->detail()->first()->description}}
                                            </div>
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="main-content-block">

                    <div class="form-group">
                        <label for="type_id">Type</label>
                        <select name="type_id">
                            <option value="1" {{ $advice_step->type_id == 1 ? 'selected' : '' }}>1 - step</option>
                            <option value="2" {{ $advice_step->type_id == 2 ? 'selected' : '' }}>2 - tip</option>                            
                            <option value="3" {{ $advice_step->type_id == 3 ? 'selected' : '' }}>3 - alert</option>                            
                        </select>
                    </div>

                    <div class="form-group">
                        <input type="checkbox" name="parameter" id="parameter" class="form-checkbox" {{ $advice_step->parameter ? 'checked' : '' }}>
                        <label for="parameter" class="checkbox {{ $advice_step->parameter ? 'active' : '' }}">Parameter</label>
                        <p class="small">Only show step if parameter matches. (Parameters are specified in alerts.)</p>
                    </div>

                    <div class="form-group">
                        <input type="checkbox" name="once" id="once" class="form-checkbox" {{ $advice_step->once ? 'checked' : '' }}>
                        <label for="once" class="checkbox {{ $advice_step->once ? 'active' : '' }}">Once</label>
                        <p class="small">Only show step once in advice. (Step needs to have multipe periodes, with same id or identifier.)</p>
                    </div>

                        <div class="form-group">
                            <input type="checkbox" name="aid" id="aid" class="form-checkbox" {{ $advice_step->aid ? 'checked' : '' }}>
                            <label for="aid" class="checkbox {{ $advice_step->aid ? 'active' : '' }}">Aid</label>
                            <p class="small">Show step in addition to other advices.</p>
                        </div>

                        <div class="form-group">
                            <input type="checkbox" name="reject" id="reject" class="form-checkbox" {{ $advice_step->reject ? 'checked' : '' }}>
                            <label for="reject" class="checkbox {{ $advice_step->reject ? 'active' : '' }}">Reject</label>
                            <p class="small">Remove this step in the advice.</p>
                        </div>

                    <div class="form-group">
                        <label for="sort_id">Sort</label>
                        <input type="number" name="sort_id" id="sort_id" class="form-control" value="{{ $advice_step->sort_id }}">
                    </div>
                    
                    <div class="form-group">
                        <label for="identifier">Identifier</label><br/>
                        <select name="identifier[]" multiple="multiple" >
                            @foreach($stepIdentifiers as $identifier)
                                <option value="{{$identifier->id}}" @if(in_array($identifier->id,$selectedIdentifiers)) selected @endif >{{$identifier->identifier}}</option>
                            @endforeach
                        </select>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="main-content-block">
                    {!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
                    <a href="{!! URL::previous() !!}" class="btn btn-default">Cancel</a>
                </div>
            </div>
        </div>
        </form>
    </div>

@stop

@section("script")
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.css">

    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.js"></script>

    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/js/bootstrap.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckeditor/ckeditor.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckeditor/adapters/jquery.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core//ckfinder/ckfinder.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core//ckfinder/ckbrowser.js') !!}"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            //CKFinder for CKEditor
            CKFinder.setupCKEditor(null, '/packages/Dcms/Core/ckfinder/');

            //CKEditor
            $("textarea.ck-text").ckeditor();

            //Bootstrap Tabs
            $(".tab-container .nav-tabs a").click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });

            //Datatable
            oTable = $('#datatable').DataTable({
                "paging": false,
                "processing": true,
                "ajax": "{{ route('admin.advice_step_products.api.table', [ $advice_step->id ?: 0]) }}",
                "columns": [
                    {data: 'information_group_id', name: 'products_information.information_group_id'},
                    {data: 'products_information', name: 'products_information.title'},
                    {data: 'catalogue', name: 'catalogue'},
                    {data: 'country', name: 'country', searchable: false},
                    {data: 'edit', name: 'edit', orderable: false, searchable: false}
                ]
            });

        });

    </script>

@stop
