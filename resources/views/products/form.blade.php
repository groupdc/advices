@extends("dcms::template/layout")

@section("content")


    <div class="main-header">
        <h1>Advice Step Products</h1>
        <ol class="breadcrumb">
            <li><a href="{!! URL::to('admin/dashboard') !!}"><i class="far fa-tachometer-alt-average"></i> Dashboard</a></li>
            <li><a href="{!! URL::to('admin/advices') !!}"><i class="far fa-pencil"></i> Advices</a></li>
            @if($advice_step_product->exists)
                <li class="active">Edit</li>
            @else
                <li class="active">Create</li>
            @endif
        </ol>
    </div>

    <div class="main-content">
        @if($advice_step_product->exists)
        <form action="{{ route('admin.advice_step_products.update', $advice_step_product->id) }}" method="post" onsubmit="return resetTables();">
            {{ method_field('PUT') }}
        @else
        <form action="{{ route('admin.advice_step_products.store') }}" method="post" onsubmit="return resetTables();">
            @if(isset($advice_to_advice_step_id))
                <input type="hidden" name="advice_to_advice_step_id" value="{{ $advice_to_advice_step_id }}">
            @endif
        @endif
        {!! csrf_field() !!}
        <div class="row">
            <div class="col-md-9">
                <div class="main-content-tab tab-container">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#data" role="tab" data-toggle="tab">Data</a></li>
                        <li><a href="#product" role="tab" data-toggle="tab">Product</a></li>
                        @yield('extratabs')
                    </ul>

                    <div class="tab-content">
                        <!-- Data - make update -->
                        <div id="data" class="tab-pane active">
                            <div class="tab-content">

                                <div class="form-group">
                                    <label for="doses">Doses</label>
                                    <input type="text" class="form-control" id="doses" name="doses" value="{{ $advice_step_product->doses }}">
                                </div>

<div class="form-group">
    <label for="doses_func">Function</label>
    <input type="text" class="form-control" id="doses_func" name="doses_func" value="{{ $advice_step_product->doses_func }}">
</div>

<div class="form-group">
    <label for="doses_func_client">Function Client</label>
    <input type="text" class="form-control" id="doses_func_client" name="doses_func_client" value="{{ $advice_step_product->doses_func_client }}">
</div>

                                <div class="form-group">
                                    <input type="checkbox" id="doses_compute" name="doses_compute" @if(!$advice_step_product->exists || $advice_step_product->doses_compute == 1) checked @endif value="1">
                                    <label for="doses_compute" class="">Doses compute</label>
                                </div>

                                <div class="form-group">
                                    <input type="checkbox" id="alternative" name="alternative" @if(!$advice_step_product->exists || $advice_step_product->alternative == 1) checked @endif value="1">
                                    <label for="alternative" class="">Alternative</label>
                                </div>

                                @if(isset($languages))
                                    <ul class="nav nav-tabs" role="tablist">
                                        @foreach($languages as $key => $language)
                                            <li class="{!! ((intval(session('overrule_default_by_language_id')) == intval($language->id)) ? 'active' : (( intval(session('overrule_default_by_language_id')) == 0 && $key == 0) ? 'active': '')) !!}">
                                            <a href="{!! '#' . $language->language . '-' . $language->country !!}" role="tab" data-toggle="tab"><img src="{!! asset('/packages/Dcms/Core/images/flag-' . strtolower($language->country) . '.svg') !!}" width="16" height="16" /> {!! $language->language_name !!}</a></li>
                                        @endforeach
                                    </ul>

                                    <div class="tab-content">
                                        @foreach($languages as $key => $information)
                                            <div id="{!! $information->language . '-' . $information->country !!}" class="tab-pane {!! ((intval(session('overrule_default_by_language_id')) == intval($information->id)) ? 'active' : ( intval(session('overrule_default_by_language_id')) == 0 && $key == 0 ? 'active': '')) !!}">
                                                <div class="form-group">
                                                    <label for="descriptions[{{$information->id}}]">Description</label>
                                                    <textarea name="descriptions[{{$information->id}}]" id="descriptions[{{$information->id}}]" rows="10" class="form-control ck-text">{{ $advice_step_product->detail->where('language_id', $information->id)->first() ? $advice_step_product->detail->where('language_id', $information->id)->first()->description : '' }}</textarea>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>

                                @endif
                            </div>
                        </div>

                        <!-- Product -->
                        <div id="product" class="tab-pane">
                            <div class="tab-content">
                                <table id="datatable_products" class="table table-hover table-condensed" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Title</th>
                                        <th>Hobby/Pro</th>
                                        <th>Information_group_id</th>
                                        <th>Country</th>
                                    </tr>
                                    </thead>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="main-content-block">
                    {!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
                    <a href="{!! URL::previous() !!}" class="btn btn-default">Cancel</a>
                </div>
            </div>
        </div>
        </form>
    </div>

@stop

@section("script")

    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/js/bootstrap.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckeditor/ckeditor.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckeditor/adapters/jquery.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core//ckfinder/ckfinder.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core//ckfinder/ckbrowser.js') !!}"></script>

    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.css">
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.js"></script>


    <script type="text/javascript">
    
        productsTable = null;
        function resetTables(){
            productsTable.search('').draw();
            return true;
        }

        $(document).ready(function () {

            //CKFinder for CKEditor
            CKFinder.setupCKEditor(null, '/packages/Dcms/Core/ckfinder/');

            //CKEditor
            $("textarea.ck-text").ckeditor();

            //Bootstrap Tabs
            $(".tab-container .nav-tabs a").click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });

            //Datatable
            productsTable = $('#datatable_products').DataTable({
                "paging": false,
                "processing": true,
                "ajax": "{{ route('admin.advice_step_products.api.table.products', [$advice_step_product->id ?: 0]) }}",
                "columns": [
                    {data: 'radio', name: 'radio'},
                    {data: 'title', name: 'title'},
                    {data: 'catalogue', name: 'catalogue'},
                    {data: 'information_group_id', name: 'information_group_id'},
                    {data: 'language', name: 'language'}
                ],
                "createdRow": function( row, data, cells ) {
                    if (~data['radio'].indexOf('checked')) {   
                    $(row).addClass('selected');
                    }
                }
            });
            
            $('#datatable_products').on('click', 'tbody tr', function (e) {
                if($(e.target).is('input[type=checkbox]')) {
                    $(this).toggleClass('selected');
                } else {
                    var clicked = $(this).find('input:checkbox');
                    if ( clicked.is(':checked')) {
                        clicked.prop('checked', false);
                    } else {
                        clicked.prop('checked', true);
                    }
        
                    $(this).toggleClass('selected');
                }
            });


        });

    </script>

@stop
