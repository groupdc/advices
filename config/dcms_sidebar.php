<?php

return [
    "Advices" => [
        "icon"  => "fa-graduation-cap",
        "links" => [
            ["route" => "admin/advices", "label" => "Advices", "permission" => "advices"],
        ],
    ],
];
