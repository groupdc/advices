<?php

namespace Dcms\Advices;

/**
*
* @author web <web@groupdc.be>
*/
use Dcms\Advices\Models\Advice;
use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

class AdvicesServiceProvider extends ServiceProvider
{
    /**
    * Indicates if loading of the provider is deferred.
    *
    * @var bool
    */
    protected $defer = false;

    public function boot()
    {
        $this->loadViewsFrom(realpath(__DIR__.'/resources/views'), 'dcms');
        $this->setupRoutes($this->app->router);

        $this->publishes([
          __DIR__.'/config/dcms_sidebar.php' => config_path('Dcms/Advices/dcms_sidebar.php'),
        ]);

        if (!is_null(config('dcms.advices'))) {
            $this->app['config']['dcms_sidebar'] =  array_merge((array)$this->app["config"]["dcms_sidebar"], config('dcms.advices.dcms_sidebar'));
        }
    }
    /**
    * Define the routes for the application.
    *
    * @param  \Illuminate\Routing\Router  $router
    * @return void
    */
    public function setupRoutes(Router $router)
    {
        $router->group(['namespace' => 'Dcms\Advices\Http\Controllers'], function ($router) {
            require __DIR__.'/Http/routes.php';
        });
    }

    public function register()
    {
        $this->registerAdvices();
    }

    private function registerAdvices()
    {
        $this->app->bind('articles', function ($app) {
            return new Advice($app);
        });
    }
}
