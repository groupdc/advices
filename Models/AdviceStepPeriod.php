<?php

namespace Dcms\Advices\Models;

use Dcms\Core\Models\EloquentDefaults;

class AdviceStepPeriod extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = 'advices_step_period';

    public function adviceSteps()
    {
        return $this->hasMany(AdviceStep::class, 'period_id', 'id');
    }
}
