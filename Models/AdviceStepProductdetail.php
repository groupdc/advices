<?php

namespace Dcms\Advices\Models;

use Illuminate\Support\Facades\Auth;
use Dcms\Core\Models\EloquentDefaults;
use Spatie\Activitylog\Traits\LogsActivity;

class AdviceStepProductdetail extends EloquentDefaults
{
    use LogsActivity;
    
    protected $connection = 'project';
    protected $table = 'products_information_group_to_advices_step_language';
    
    protected $fillable = ['products_information_group_to_advices_step_id', 'language_id'];
    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = [ 'updated_at', 'created_at'];

    protected static $logName = 'table [products_information_group_to_advices_step_language]';
    protected static $logOnlyDirty = true;
   
    public function getDescriptionForEvent(string $eventName): string
    {
        $user = "unknown";
        if (isset(Auth::guard('dcms')->user()->username)) {
            $user = Auth::guard('dcms')->user()->username;
        }

        return $eventName .  " - user: ".$user;
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        ->dontSubmitEmptyLogs();
        // Chain fluent methods for configuration options
    }
}
