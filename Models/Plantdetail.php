<?php

namespace Dcms\Advices\Models;

use Dcms\Core\Models\EloquentDefaults;

class Plantdetail extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "plants_language";

    public function plant()
    {
        return $this->belongsTo(Plant::class, 'plant_id', 'id');
    }

    public function plantcategory()
    {
        return $this->belongsTo(Plantcategory::class, 'category_id', 'id');
    }

    public function language()
    {
        return $this->belongsTo('Dcweb\Dcms\Models\Languages\Language', 'language_id', 'id');
    }
}
