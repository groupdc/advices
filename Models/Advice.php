<?php

namespace Dcms\Advices\Models;

use Illuminate\Support\Facades\Auth;
use Dcms\Core\Models\EloquentDefaults;
use Spatie\Activitylog\Traits\LogsActivity;

class Advice extends EloquentDefaults
{
    use LogsActivity;

    protected $connection = 'project';
    protected $table = 'advices';

    protected $fillable = ['pro','nursing','planting', 'seed','seedling', 'outdoor', 'indoor', 'greenhouse', 'in_ground', 'pot', 'container','raisedbed'];
    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = [ 'updated_at', 'created_at'];

    protected static $logName = 'table [advices]';
    protected static $logOnlyDirty = true;
   
    public function getDescriptionForEvent(string $eventName): string
    {
        $user = "unknown";
        if (isset(Auth::guard('dcms')->user()->username)) {
            $user = Auth::guard('dcms')->user()->username;
        }

        return $eventName .  " - user: ".$user;
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        ->dontSubmitEmptyLogs();
        // Chain fluent methods for configuration options
    }

    public function plants()
    {
        return $this->belongsToMany(Plant::class, 'plants_to_advices', 'advices_id', 'plants_id')->using('Dcms\Advices\Models\Plants_To_Advices')->withTimestamps();
    }

    public function conditions()
    {
        return $this->belongsToMany(Condition::class, 'conditions_to_advices', 'advices_id', 'conditions_id')->using('Dcms\Advices\Models\Conditions_To_Advices')->withTimestamps();
    }

    public function adviceSteps()
    {
        return $this->belongsToMany(AdviceStep::class, 'advices_to_advices_step', 'advices_id', 'advices_step_id')
                    ->using(AdviceToAdviceStep::class)
                    ->withPivot('id', 'period_id')
                    ->withTimestamps();
    }
}
