<?php

namespace Dcms\Advices\Models;

use Dcms\Core\Models\EloquentDefaults;

class Plantcategorydetail extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "plants_categories_language";

    public function plantcategory()
    {
        return $this->belongsTo(Plantcategory::class, 'category_id', 'id');
    }

    public function language()
    {
        return $this->belongsTo('Dcweb\Dcms\Models\Languages\Language', 'language_id', 'id');
    }
}
