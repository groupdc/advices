<?php

namespace Dcms\Advices\Models;

use Illuminate\Support\Facades\Auth;
use Dcms\Core\Models\EloquentDefaults;
use Spatie\Activitylog\Traits\LogsActivity;

class AdviceStep extends EloquentDefaults
{
    use LogsActivity;
    
    protected $connection = 'project';
    protected $table = 'advices_step';

    protected $fillable = ['sort_id','type_id','reject', 'always','parameter', 'once', 'aid'];
    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = [ 'updated_at', 'created_at'];

    protected static $logName = 'table [advices_step]';
    protected static $logOnlyDirty = true;

    public function getDescriptionForEvent(string $eventName): string
    {
        $user = "unknown";
        if (isset(Auth::guard('dcms')->user()->username)) {
            $user = Auth::guard('dcms')->user()->username;
        }

        return $eventName .  " - user: ".$user;
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        ->dontSubmitEmptyLogs();
        // Chain fluent methods for configuration options
    }

    public function advices()
    {
        return $this->belongsToMany(Advice::class, 'advices_to_advices_step', 'advices_step_id', 'advices_id')
                    ->using(AdviceToAdviceStep::class)
                    ->withPivot('id', 'period_id')
                    ->withTimestamps();
    }

    public function adviceStepsAlert()
    {
        //The third argument is the foreign key name of the model on which you are defining the relationship, while the fourth argument is the foreign key name of the model that you are joining to:
        return $this->belongsToMany(AdvicesStepAlert::class, 'advices_step_to_advices_step_alert', 'advices_step_id', 'advices_step_alert_id')
                    ->using(AdviceToAdviceStep::class)
                    ->withTimestamps();
    }

    public function stepIdentifiers()
    {
        //The third argument is the foreign key name of the model on which you are defining the relationship, while the fourth argument is the foreign key name of the model that you are joining to:
        return $this->belongsToMany(AdvicesStepIdentifier::class, 'advices_step_to_advices_step_identifier', 'advices_step_id', 'advices_step_identifier_id')
                    ->withTimestamps();
    }

    public function detail()
    {
        return $this->hasMany(AdviceStepdetail::class, 'advices_step_id', 'id');
    }

    public function product()
    {
        return $this->hasMany(AdviceStepProduct::class, 'advices_step_id', 'id');
    }
}
