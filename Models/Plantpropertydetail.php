<?php

namespace Dcms\Advices\Models;

use Dcms\Core\Models\EloquentDefaults;

class Plantpropertydetail extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "plants_property_language";

    public function Plantproperty()
    {
        return $this->belongsTo(Plantproperty::class, 'property_id', 'id');
    }
}
