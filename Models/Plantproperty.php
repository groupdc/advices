<?php

namespace Dcms\Advices\Models;

use Dcms\Core\Models\EloquentDefaults;

class Plantproperty extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "plants_property";

    public function plantpropertydetail()
    {
        return $this->hasMany(Plantpropertydetail::class, 'property_id', 'id');
    }
}
