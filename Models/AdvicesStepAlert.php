<?php

namespace Dcms\Advices\Models;

use Dcms\Core\Models\EloquentDefaults;

class AdvicesStepAlert extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = 'advices_step_alert';

    public function detail()
    {
        return $this->hasMany(AdviceStepAlertdetail::class, 'advices_step_alert_id', 'id');
    }

    public function adviceSteps()
    {
        //The third argument is the foreign key name of the model on which you are defining the relationship, while the fourth argument is the foreign key name of the model that you are joining to:
        return $this->belongsToMany(AdviceStep::class, 'advices_step_to_advices_step_alert', 'advices_step_alert_id', 'advices_step_id')
                    ->using(AdviceToAdviceStep::class)
                    ->withTimestamps();
    }

    public function parameter()
    {
        return $this->hasOne(AdvicesStepParameter::class, 'id', 'parameter_id');
    }
}
