<?php

namespace Dcms\Advices\Models;

use Dcms\Core\Models\EloquentDefaults;

class Conditiondetail extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = 'conditions_language';

    public function condition()
    {
        return $this->belongsTo(Condition::class, 'conditions_id', 'id');
    }

    public function language()
    {
        return $this->belongsTo('Dcweb\Dcms\Models\Languages\Language', 'language_id', 'id');
    }
}
