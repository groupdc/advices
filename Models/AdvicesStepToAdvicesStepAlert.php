<?php

namespace Dcms\Advices\Models;

use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Relations\Pivot;

class AdvicesStepToAdvicesStepAlert extends Pivot
{
    use LogsActivity;

    protected $connection = 'project';
    protected $table = 'advices_step_to_advices_step_alert';

    protected $fillable = ['advices_step_id', 'advices_step_alert_id'];
    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = [ 'updated_at', 'created_at'];

    protected static $logName = 'table [advices_step_to_advices_step_alert]';
    protected static $logOnlyDirty = true;
    public $incrementing = true;
    
    public function getDescriptionForEvent(string $eventName): string
    {
        $user = "unknown";
        if (isset(Auth::guard('dcms')->user()->username)) {
            $user = Auth::guard('dcms')->user()->username;
        }

        return $eventName .  " - user: ".$user;
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        ->dontSubmitEmptyLogs();
        // Chain fluent methods for configuration options
    }

    public function AdviceStep()
    {
        return $this->belongsTo(AdviceStep::class, 'advices_step_id', 'id');
    }

    public function adviceStepAlert()
    {
        return $this->belongsTo(AdviceStepAlert::class, 'advices_step_alert_id', 'id');
    }
}
