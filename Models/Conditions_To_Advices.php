<?php

namespace Dcms\Advices\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Spatie\Activitylog\Traits\LogsActivity;

class Conditions_To_Advices extends Pivot
{
    use LogsActivity;

    protected $connection = 'project';
    protected $table = 'conditions_to_advices';

    protected $fillable = ['conditions_id', 'advices_id'];
    protected static $logAttributes = ['conditions_id', 'advices_id'];
    protected static $logAttributesToIgnore = [ 'updated_at', 'created_at'];

    protected static $logName = 'table [conditions_to_advices]';
    protected static $logOnlyDirty = true;
    public $incrementing = true;
    
    public function getDescriptionForEvent(string $eventName): string
    {
        $user = "unknown";
        if (isset(Auth::guard('dcms')->user()->username)) {
            $user = Auth::guard('dcms')->user()->username;
        }

        return $eventName .  " - user: ".$user;
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        ->dontSubmitEmptyLogs();
        // Chain fluent methods for configuration options
    }
}
