<?php

namespace Dcms\Advices\Models;

use Dcms\Core\Models\EloquentDefaults;

class Plantcategory extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "plants_categories";

    public function plantcategorydetail()
    {
        return $this->belongsTo(Plantcategorydetail::class, 'id');
    }

    public function plants()
    {
        return $this->hasMany(Plant::class, 'category_id', 'id');
    }
}
