<?php

namespace Dcms\Advices\Models;

use Dcms\Core\Models\EloquentDefaults;

class AdvicesStepParameter extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = 'advices_step_parameter';
}
