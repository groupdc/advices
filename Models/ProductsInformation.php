<?php

namespace Dcms\Advices\Models;

use Dcms\Core\Models\EloquentDefaults;

class ProductsInformation extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "products_information";
    protected $fillable = array('language_id', 'title', 'description');

    protected function setPrimaryKey($key)
    {
        $this->primaryKey = $key;
    }

    public function products()
    {
        /*
        The first argument in belongsToMany() is the name of the class Productdata, the second argument is the name of the pivot table, followed by the name of the product_id column, and at last the name of the product_data_id column.
        */
        return $this->belongsToMany('\Dcweb\Dcms\Models\Products\Product', 'products_to_products_information', 'product_information_id', 'product_id');
    }

    public function productcategory()
    {
        return $this->belongsTo('\Dcweb\Dcms\Models\Products\Category', 'product_category_id', 'id');
    }

    public function otherlanguages()
    {
        //return $this->hasMany('-MODEL-', 'foreign_key', 'local_key');
        return $this->hasMany('\Dcweb\Dcms\Models\Products\Information', 'information_group_id', 'information_group_id')->whereNotNull('information_group_id');
    }

    public function pages()
    {
        //ISSUE
        //https://github.com/laravel/framework/issues/5418

        // SOLUTION
        //http://stackoverflow.com/questions/24391759/laravel-belongstomany-relationship-defining-local-key-on-both-tables

        // BelongsToMany belongsToMany(string $related, string $table = null, string $foreignKey = null, string $otherKey = null, string $relation = null)
        return $this->belongsToMany('Dcweb\Dcms\Models\Pages\Pageslanguage', 'pages_to_products_information_group', 'information_id', 'page_id');

        /*$this->setPrimaryKey('information_group_id');
        $relation = $this->belongsToMany('Dcweb\Dcms\Models\Articles\Article', 'articles_to_products_information_group', 'information_group_id', 'article_id');
        $this->setPrimaryKey('id');
        return $relation;*/
    }

    public function articles()
    {
        //ISSUE
        //https://github.com/laravel/framework/issues/5418

        // SOLUTION
        //http://stackoverflow.com/questions/24391759/laravel-belongstomany-relationship-defining-local-key-on-both-tables

        // BelongsToMany belongsToMany(string $related, string $table = null, string $foreignKey = null, string $otherKey = null, string $relation = null)
        return $this->belongsToMany('Dcweb\Dcms\Models\Articles\Article', 'articles_to_products_information_group', 'information_id', 'article_id');

        /*$this->setPrimaryKey('information_group_id');
        $relation = $this->belongsToMany('Dcweb\Dcms\Models\Articles\Article', 'articles_to_products_information_group', 'information_group_id', 'article_id');
        $this->setPrimaryKey('id');
        return $relation;*/
    }

    public function plants()
    {
        //ISSUE
        //https://github.com/laravel/framework/issues/5418

        // SOLUTION
        //http://stackoverflow.com/questions/24391759/laravel-belongstomany-relationship-defining-local-key-on-both-tables

        // BelongsToMany belongsToMany(string $related, string $table = null, string $foreignKey = null, string $otherKey = null, string $relation = null)
        return $this->belongsToMany('App\Models\Admin\DCM\Plant', 'products_information_group_to_plants', 'information_id', 'plant_id');

        /*$this->setPrimaryKey('information_group_id');
        $relation = $this->belongsToMany('Dcweb\Dcms\Models\Articles\Article', 'articles_to_products_information_group', 'information_group_id', 'article_id');
        $this->setPrimaryKey('id');
        return $relation;*/
    }
}
