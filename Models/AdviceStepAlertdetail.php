<?php

namespace Dcms\Advices\Models;

use Dcms\Core\Models\EloquentDefaults;

class AdviceStepAlertdetail extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = 'advices_step_alert_language';
    protected $fillable = ['advices_step_alert_id', 'language_id'];
}
