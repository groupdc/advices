<?php

namespace Dcms\Advices\Models;

use Dcms\Core\Models\EloquentDefaults;

class Plant extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = "plants";

    public function plantdetail()
    {
        return $this->hasMany(Plantdetail::class, 'plant_id', 'id');
    }

    public function productinformation()
    {
        // BelongsToMany belongsToMany(string $related, string $table = null, string $foreignKey = null, string $otherKey = null, string $relation = null)
    }

    public function plantproperty()
    {
        /*
        The first argument in belongsToMany() is the name of the class Productdata, the second argument is the name of the pivot table, followed by the name of the product_id column, and at last the name of the product_data_id column.
        */
        return $this->belongsToMany(Plantproperty::class, 'plants_to_property', 'plant_id', 'plant_property_id');
    }
}
