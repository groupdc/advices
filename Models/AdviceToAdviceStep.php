<?php

namespace Dcms\Advices\Models;

use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Relations\Pivot;

class AdviceToAdviceStep extends Pivot
{
    use LogsActivity;
    
    protected $connection = 'project';
    protected $table = 'advices_to_advices_step';

    protected $fillable = ['advices_id', 'advices_step_id', 'period_id'];
    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = [ 'updated_at', 'created_at'];

    protected static $logName = 'table [advices_to_advices_step]';
    protected static $logOnlyDirty = true;
    public $incrementing = true;

    public function getDescriptionForEvent(string $eventName): string
    {
        $user = "unknown";
        if (isset(Auth::guard('dcms')->user()->username)) {
            $user = Auth::guard('dcms')->user()->username;
        }

        return $eventName .  " - user: ".$user;
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        ->dontSubmitEmptyLogs();
        // Chain fluent methods for configuration options
    }

    public function period()
    {
        return $this->belongsTo(AdviceStepPeriod::class, 'period_id', 'id');
    }

    public function advice()
    {
        return $this->belongsTo(Advice::class, 'advices_id', 'id');
    }

    public function adviceStep()
    {
        return $this->belongsTo(AdviceStep::class, 'advices_step_id', 'id');
    }

    public function getCreatedAtColumn()
    {
        return 'created_at';
    }

    public function getUpdatedAtColumn()
    {
        return 'updated_at';
    }
}
