<?php

namespace Dcms\Advices\Models;

use Dcms\Core\Models\EloquentDefaults;

class Condition extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = 'conditions';

    public function detail()
    {
        return $this->hasMany(Conditiondetail::class, 'conditions_id', 'id');
    }
}
