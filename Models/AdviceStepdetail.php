<?php

namespace Dcms\Advices\Models;

use Illuminate\Support\Facades\Auth;
use Dcms\Core\Models\EloquentDefaults;
use Spatie\Activitylog\Traits\LogsActivity;

class AdviceStepdetail extends EloquentDefaults
{
    use LogsActivity;

    protected $connection = 'project';
    protected $table = 'advices_step_language';

    protected $fillable = ['advices_step_id', 'language_id','title','description'];
    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = [ 'updated_at', 'created_at'];

    protected static $logName = 'table [advices_step_language]';
    protected static $logOnlyDirty = true;
    
    public function getDescriptionForEvent(string $eventName): string
    {
        $user = "unknown";
        if (isset(Auth::guard('dcms')->user()->username)) {
            $user = Auth::guard('dcms')->user()->username;
        }

        return $eventName .  " - user: ".$user;
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        ->dontSubmitEmptyLogs();
        // Chain fluent methods for configuration options
    }
}
